
import sys
import dj_database_url

TESTING = (sys.argv[1:2] == ['test'])

SOUTH_TESTS_MIGRATE = False # don't migrate test database

if not TESTING:
    DATABASES = {
        'default': dj_database_url.config(
            default='postgres://localhost'
        )
    }
